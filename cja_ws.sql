-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 13, 2017 at 03:36 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cja_ws`
--

-- --------------------------------------------------------

--
-- Table structure for table `cleaningjob`
--

CREATE TABLE `cleaningjob` (
  `id_job` int(11) NOT NULL,
  `id_staff` int(4) NOT NULL,
  `nomor_ruang` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cleaningjob`
--

INSERT INTO `cleaningjob` (`id_job`, `id_staff`, `nomor_ruang`) VALUES
(1, 1001, 7601),
(6, 1005, 9017);

-- --------------------------------------------------------

--
-- Table structure for table `cleaningstaff`
--

CREATE TABLE `cleaningstaff` (
  `id_staff` int(4) NOT NULL,
  `nama_staff` varchar(30) NOT NULL,
  `nomorhp` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cleaningstaff`
--

INSERT INTO `cleaningstaff` (`id_staff`, `nama_staff`, `nomorhp`) VALUES
(1001, 'Dwi Muhammad', '081265739364'),
(1002, 'Tri Putra', '081532786530'),
(1003, 'Dian Indah', '082234560976'),
(1004, 'Agus Nur Ade', '081354012976'),
(1005, 'Reza Andirio', '081574981028'),
(1006, 'Fitri Sitiah', '081265739364'),
(1007, 'Andy Yudiko', '081532786530'),
(1008, 'Arya Ika', '082234560976'),
(1009, 'Maya Lestari', '081354012976'),
(1010, 'Dian Sri', '081574981028'),
(1011, 'Tyas Sari', '081265739364'),
(1012, 'Ajie Kurniawan', '081532786530'),
(1013, 'Arif Eko', '082234560976'),
(1014, 'Indra Wahyu', '081354012976'),
(1022, 'Junaldi', '081398761234');

-- --------------------------------------------------------

--
-- Table structure for table `ruangkuliah`
--

CREATE TABLE `ruangkuliah` (
  `nomor_ruang` int(4) NOT NULL,
  `lokasi_ruang` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ruangkuliah`
--

INSERT INTO `ruangkuliah` (`nomor_ruang`, `lokasi_ruang`) VALUES
(2201, 'SITH'),
(2202, 'SITH'),
(2203, 'SITH'),
(3101, 'Sipil'),
(3102, 'Sipil'),
(3201, 'Sipil'),
(3202, 'Sipil'),
(3203, 'Sipil'),
(3204, 'Sipil'),
(4101, 'Gedung FTMD lantai 1'),
(4102, 'Gedung FTMD lantai 1'),
(4103, 'Gedung FTMD lantai 4'),
(4104, 'Gedung FTMD lantai 4'),
(4105, 'Gedung FTMD lantai 4'),
(4106, 'Gedung FTMD lantai 4'),
(6101, 'Arsitektur / Labtek IX B'),
(6102, 'Arsitektur / Labtek IX B'),
(6302, 'Planologi / Labtek IX A'),
(6303, 'Planologi / Labtek IX A'),
(6305, 'Planologi / Labtek IX A'),
(6306, 'Planologi / Labtek IX A'),
(7601, 'Labtek 5 (IF)'),
(7602, 'Labtek 5 (IF)'),
(7603, 'Labtek 5 (IF)'),
(7604, 'Labtek 5 (IF)'),
(9008, 'Teknik Lingkungan (Gedung Lama'),
(9009, 'Liga Film Mahasiswa'),
(9012, 'Lantai Dasar Labtek 7 Farmasi'),
(9013, 'Lantai Dasar Labtek 7 Farmasi'),
(9016, 'Oktagon'),
(9017, 'Oktagon'),
(9018, 'Oktagon'),
(9019, 'Oktagon'),
(9022, 'TVST'),
(9023, 'TVST'),
(9024, 'TVST'),
(9103, 'GKU Barat'),
(9104, 'GKU Barat'),
(9106, 'GKU Barat'),
(9107, 'GKU Barat'),
(9212, 'GKU Timur'),
(9213, 'GKU Timur'),
(9214, 'GKU Timur'),
(9221, 'GKU Timur'),
(9301, 'Labtek 6 Lantai Dasar (selasar'),
(9302, 'Labtek 6 Lantai Dasar (selasar'),
(9303, 'Labtek 6 Lantai Dasar (selasar'),
(9304, 'Labtek 6 Lantai Dasar (selasar'),
(9401, 'Gedung Labtek 1 lantai 2'),
(9402, 'Gedung Labtek 1 lantai 2'),
(9403, 'Gedung Labtek 1 lantai 2'),
(9404, 'Gedung Labtek 1 lantai 2');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cleaningjob`
--
ALTER TABLE `cleaningjob`
  ADD PRIMARY KEY (`id_job`),
  ADD KEY `id_staff` (`id_staff`),
  ADD KEY `nomor_ruang` (`nomor_ruang`);

--
-- Indexes for table `cleaningstaff`
--
ALTER TABLE `cleaningstaff`
  ADD PRIMARY KEY (`id_staff`);

--
-- Indexes for table `ruangkuliah`
--
ALTER TABLE `ruangkuliah`
  ADD PRIMARY KEY (`nomor_ruang`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cleaningjob`
--
ALTER TABLE `cleaningjob`
  MODIFY `id_job` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `cleaningstaff`
--
ALTER TABLE `cleaningstaff`
  MODIFY `id_staff` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1023;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cleaningjob`
--
ALTER TABLE `cleaningjob`
  ADD CONSTRAINT `cleaningjob_ibfk_1` FOREIGN KEY (`id_staff`) REFERENCES `cleaningstaff` (`id_staff`),
  ADD CONSTRAINT `cleaningjob_ibfk_2` FOREIGN KEY (`nomor_ruang`) REFERENCES `ruangkuliah` (`nomor_ruang`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
