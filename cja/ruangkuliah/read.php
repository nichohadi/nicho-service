<?php

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once '../config/Db.php';
include_once '../object/ruangkuliah.php';

// instantiate database and ruangkuliah object
$database = new Db();
$db = $database->getConnection();

// initialize object
$ruangkuliah = new RuangKuliah($db);

// query ruangkuliah
$stmt = $ruangkuliah->read();
$num = $stmt->rowCount();

// check if more than 0 record found
if ($num > 0) {
    // ruangkuliah array
    $ruangkuliah_arr = array();
    $ruangkuliah_arr["records"] = array();

    // retrieve table contents
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        // extract row
        extract($row);
        $ruangkuliah_item = array(
            "ruang" => $row['nomor_ruang'],
            "lokasi" => $row['lokasi_ruang']
        );
        array_push($ruangkuliah_arr["records"], $ruangkuliah_item);
    }
    echo json_encode($ruangkuliah_arr);
} else {
    echo json_encode(
            array("message" => "Tidak ada data ruang kuliah.")
    );
}
?>
