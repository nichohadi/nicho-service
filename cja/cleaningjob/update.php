<?php

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: PUT");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// include database and object files
include_once '../config/Db.php';
include_once '../object/cleaningjob.php';

$database = new Db();
$db = $database->getConnection();

// initialize object
$cleaningjob = new CleaningJob($db);

// get posted data
$data = json_decode(file_get_contents("php://input", true));

// set ID property of cleaningjob to be updated
$cleaningjob->idjob = $data->idjob;
// set cleaningstaff property value
$cleaningjob->idstaff = $data->idstaff;
$cleaningjob->ruang = $data->ruang;

// update the cleaningjob
if ($cleaningjob->update()) {
    echo '{';
    echo '"message": "Data cleaning job berhasil diperbaharui."';
    echo '}';
}

// if unable to update the cleaningjob, tell the user
else {
    echo '{';
    echo '"message": "Data cleaning job tidak berhasil diperbaharui."';
    echo '}';
}
