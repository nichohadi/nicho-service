<?php

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");


// include database and object files
include_once '../config/Db.php';
include_once '../object/cleaningjob.php';

$database = new Db();
$db = $database->getConnection();

// initialize object
$cleaningjob = new CleaningJob($db);

// set ID property of cleaningjob to be deleted
$cleaningjob->idjob = filter_input(INPUT_GET, 'idjob');

// delete the cleaningjob
if ($cleaningjob->delete()) {
    echo '{';
    echo '"message": "Data cleaning job berhasil dihapus."';
    echo '}';
}

// if unable to delete the cleaningjob
else {
    echo '{';
    echo '"message": "Data cleaning job tidak berhasil dihapus."';
    echo '}';
}
?>
