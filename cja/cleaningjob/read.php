<?php

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once '../config/Db.php';
include_once '../object/cleaningjob.php';

// instantiate database and cleaningjob object
$database = new Db();
$db = $database->getConnection();

// initialize object
$cleaningjob = new CleaningJob($db);

//query cleaningjob
if (isset($_GET["idjob"])) {
  $stmt = $cleaningjob->readidjob($_GET["idjob"]);
}
else {
  $stmt = $cleaningjob->read();
}

$num = $stmt->rowCount();

// check if more than 0 record found
if ($num > 0) {
    // cleaningjob array
    $cleaningjob_arr = array();
    $cleaningjob_arr["records"] = array();

    // retrieve table contents
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        // extract row
        extract($row);
        $cleaningjob_item = array(
            "idjob" => $row['id_job'],
            "idstaff" => $row['id_staff'],
            "nama" => $row['nama_staff'],
            "nomor" => $row['nomorhp'],
            "ruang" => $row['nomor_ruang'],
        );
        array_push($cleaningjob_arr["records"], $cleaningjob_item);
    }
    echo json_encode($cleaningjob_arr);
} else {
    echo json_encode(
            array("message" => "Tidak ada data cleaning job.")
    );
}
?>
