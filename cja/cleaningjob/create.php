<?php

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// include database and object files
include_once '../config/Db.php';
include_once '../object/cleaningjob.php';

$database = new Db();
$db = $database->getConnection();

// initialize object
$cleaningjob = new CleaningJob($db);

// get posted data
$data = json_decode(file_get_contents("php://input", true));

// set cleaningjob property value
$cleaningjob->idstaff = $data->idstaff;
$cleaningjob->ruang = $data->ruang;

// create the cleaningjob
if ($cleaningjob->create()) {
    echo '{';
    echo '"message": "Data cleaning job berhasil ditambahkan."';
    echo '}';
}

// if unable to create the cleaningjob, tell the user
else {
    echo '{';
    echo '"message": "Data cleaning job tidak berhasil ditambahkan."';
    echo '}';
}
