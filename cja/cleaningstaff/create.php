<?php

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// include database and object files
include_once '../config/Db.php';
include_once '../object/cleaningstaff.php';

$database = new Db();
$db = $database->getConnection();

// initialize object
$cleaningstaff = new CleaningStaff($db);

// get posted data
$data = json_decode(file_get_contents("php://input", true));

// set cleaningstaff property value
$cleaningstaff->nama = $data->nama;
$cleaningstaff->nomor = $data->nomor;

// create the cleaningstaff
if ($cleaningstaff->create()) {
    echo '{';
    echo '"message": "Data staf berhasil ditambahkan."';
    echo '}';
}

// if unable to create the cleaningstaff, tell the user
else {
    echo '{';
    echo '"message": "Data staf tidak berhasil ditambahkan."';
    echo '}';
}
