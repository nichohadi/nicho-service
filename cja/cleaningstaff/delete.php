<?php

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");


// include database and object files
include_once '../config/Db.php';
include_once '../object/cleaningstaff.php';

$database = new Db();
$db = $database->getConnection();

// initialize object
$cleaningstaff = new CleaningStaff($db);

// set ID property of cleaningstaff to be deleted
$cleaningstaff->id = filter_input(INPUT_GET, 'id');

// delete the cleaningstaff
if ($cleaningstaff->delete()) {
    echo '{';
    echo '"message": "Data staf berhasil dihapus."';
    echo '}';
}

// if unable to delete the cleaningstaff
else {
    echo '{';
    echo '"message": "Data staf tidak berhasil dihapus."';
    echo '}';
}
?>
