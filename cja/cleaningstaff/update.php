<?php

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: PUT");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// include database and object files
include_once '../config/Db.php';
include_once '../object/cleaningstaff.php';

$database = new Db();
$db = $database->getConnection();

// initialize object
$cleaningstaff = new CleaningStaff($db);

// get posted data
$data = json_decode(file_get_contents("php://input", true));

// set ID property of cleaningstaff to be updated
$cleaningstaff->id = $data->id;
// set cleaningstaff property value
$cleaningstaff->nama = $data->nama;
$cleaningstaff->nomor = $data->nomor;

// update the cleaningstaff
if ($cleaningstaff->update()) {
    echo '{';
    echo '"message": "Data staf berhasil diperbaharui."';
    echo '}';
}

// if unable to update the cleaningstaff, tell the user
else {
    echo '{';
    echo '"message": "Data staf tidak berhasil diperbaharui."';
    echo '}';
}
