<?php

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once '../config/Db.php';
include_once '../object/cleaningstaff.php';

// instantiate database and department object
$database = new Db();
$db = $database->getConnection();

// initialize object
$cleaningstaff = new CleaningStaff($db);

// query cleaningstaff
$stmt = $cleaningstaff->read();
$num = $stmt->rowCount();

// check if more than 0 record found
if ($num > 0) {
    // cleaningstaff array
    $cleaningstaff_arr = array();
    $cleaningstaff_arr["records"] = array();

    // retrieve table contents
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        // extract row
        extract($row);
        $cleaningstaff_item = array(
            "id" => $row['id_staff'],
            "nama" => $row['nama_staff'],
            "nomor" => $row['nomorhp'],
        );
        array_push($cleaningstaff_arr["records"], $cleaningstaff_item);
    }
    echo json_encode($cleaningstaff_arr);
} else {
    echo json_encode(
            array("message" => "Tidak ada data staf cleaning.")
    );
}
?>
