<?php

class CleaningJob {

    // database connection and table name
    private $conn;
    private $table_name = "cleaningjob";
    // object properties
    public $idjob;
    public $idstaff;
    public $ruang;

    // constructor with $db as database connection
    public function __construct($db) {
        $this->conn = $db;
    }

    // read cleaningjob
    function read() {
        // query to select all
        $query = "SELECT id_job, id_staff, nama_staff, nomorhp, nomor_ruang
            FROM
                " . $this->table_name . " NATURAL JOIN cleaningstaff
            ORDER BY
                id_job";
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
        return $stmt;
    }

    function readidjob($idjob) {
        // query to select all
        $query = "SELECT id_job, id_staff, nama_staff, nomorhp, nomor_ruang
            FROM
                " . $this->table_name . " NATURAL JOIN cleaningstaff
            WHERE
                id_job = " . $idjob . "
            ORDER BY
                id_job";
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
        return $stmt;
    }

    // create cleaningjob
    function create() {
        // query to insert record
        $query = "INSERT INTO
                " . $this->table_name . "
            SET
                id_staff=:idstaff,
                nomor_ruang=:ruang";
        // prepare query
        $stmt = $this->conn->prepare($query);
        // sanitize
        $this->idstaff = htmlspecialchars(strip_tags($this->idstaff));
        $this->ruang = htmlspecialchars(strip_tags($this->ruang));

        // bind values
        $stmt->bindParam(":idstaff", $this->idstaff);
        $stmt->bindParam(":ruang", $this->ruang);

        // execute query
        if ($stmt->execute()) {
            return true;
        } else {
            return false;
        }
    }

    // update the cleaningjob
    function update() {
        // update query
        $query = "UPDATE
                " . $this->table_name . "
            SET
                id_staff = :idstaff,
                nomor_ruang = :ruang
            WHERE
                id_job = :idjob";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // sanitize
        $this->idstaff = htmlspecialchars(strip_tags($this->idstaff));
        $this->ruang = htmlspecialchars(strip_tags($this->ruang));
        $this->idjob = htmlspecialchars(strip_tags($this->idjob));

        // bind new values
        $stmt->bindParam(':idstaff', $this->idstaff);
        $stmt->bindParam(':ruang', $this->ruang);
        $stmt->bindParam(':idjob', $this->idjob);

        // execute the query
        if ($stmt->execute()) {
            return true;
        } else {
            return false;
        }
    }

    // delete the cleaningjob
        function delete() {
            // delete query
            $query = "DELETE FROM " . $this->table_name . " WHERE id_job = ?";

            // prepare query
            $stmt = $this->conn->prepare($query);

            // sanitize
            $this->idjob = htmlspecialchars(strip_tags($this->idjob));

            // bind id of record to delete
            $stmt->bindParam(1, $this->idjob);

            // execute query
            if ($stmt->execute()) {
                return true;
            }
            return false;
        }

}
