<?php

class CleaningStaff {

    // database connection and table name
    private $conn;
    private $table_name = "cleaningstaff";
    // object properties
    public $id;
    public $nama;
    public $nomor;

    // constructor with $db as database connection
    public function __construct($db) {
        $this->conn = $db;
    }

    // read cleaningstaff
    function read() {
        // query to select all
        $query = "SELECT d.id_staff, d.nama_staff, d.nomorhp
            FROM
                " . $this->table_name . " d
            ORDER BY
                d.id_staff";
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
        return $stmt;
    }

    // create cleaningstaff
    function create() {
        // query to insert record
        $query = "INSERT INTO
                " . $this->table_name . "
            SET
                nama_staff=:nama,
                nomorhp=:nomor";
        // prepare query
        $stmt = $this->conn->prepare($query);
        // sanitize
        $this->nama = htmlspecialchars(strip_tags($this->nama));
        $this->nomor = htmlspecialchars(strip_tags($this->nomor));

        // bind values
        $stmt->bindParam(":nama", $this->nama);
        $stmt->bindParam(":nomor", $this->nomor);

        // execute query
        if ($stmt->execute()) {
            return true;
        } else {
            return false;
        }
    }

    // update the cleaningstaff
    function update() {
        // update query
        $query = "UPDATE
                " . $this->table_name . "
            SET
                nama_staff = :nama,
                nomorhp = :nomor
            WHERE
                id_staff = :id";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // sanitize
        $this->nama = htmlspecialchars(strip_tags($this->nama));
        $this->nomor = htmlspecialchars(strip_tags($this->nomor));
        $this->id = htmlspecialchars(strip_tags($this->id));

        // bind new values
        $stmt->bindParam(':nama', $this->nama);
        $stmt->bindParam(':nomor', $this->nomor);
        $stmt->bindParam(':id', $this->id);

        // execute the query
        if ($stmt->execute()) {
            return true;
        } else {
            return false;
        }
    }

    // delete the cleaningstaff
        function delete() {
            // delete query
            $query = "DELETE FROM " . $this->table_name . " WHERE id_staff = ?";

            // prepare query
            $stmt = $this->conn->prepare($query);

            // sanitize
            $this->id = htmlspecialchars(strip_tags($this->id));

            // bind id of record to delete
            $stmt->bindParam(1, $this->id);

            // execute query
            if ($stmt->execute()) {
                return true;
            }
            return false;
        }

}
