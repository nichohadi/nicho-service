<?php

class RuangKuliah {

    // database connection and table name
    private $conn;
    private $table_name = "ruangkuliah";
    // object properties
    public $ruang;
    public $lokasi;

    // constructor with $db as database connection
    public function __construct($db) {
        $this->conn = $db;
    }

    // read ruangkuliah
    function read() {
        // query to select all
        $query = "SELECT d.nomor_ruang, d.lokasi_ruang
            FROM
                " . $this->table_name . " d
            ORDER BY
                d.nomor_ruang";
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
        return $stmt;
    }

}
